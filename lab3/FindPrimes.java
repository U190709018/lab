public class FindPrimes{
    
    public static boolean isPrime(int value){
        int test = 2;
        if(value == 2)
            return true;
        while(test < value){
                if(value % 2 == 0)
                    return false;
                else if(value % test == 0)
                    return false;
                else if(value % test != 0)
                    test += 1;
        }
        return true;
    }

    public static void main(String[] args){
        int until_number = Integer.parseInt(args[0]);
        int value1 = 2;
        String prime_numbers = "";
        while(value1 < until_number){
            if(isPrime(value1))
                prime_numbers = prime_numbers + " " + value1;
            value1 += 1;
        }
        System.out.println(prime_numbers);
    }
}
             
