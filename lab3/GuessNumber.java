import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number =rand.nextInt(100); //generates a number between 0 and 99
		
		
		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.println("Can you guess it: ");
		
        boolean condition = true;
        int attempt = 0;
        do{
		    int guess = reader.nextInt(); //Read the user input
            attempt += 1;        
            if(guess == number){
                System.out.println("Congratulations!!! you have found the number after " + attempt + " attempts");
                condition = false;
            }
            else if(guess == -1){
                System.out.println("Sorry the number was " + number);
                condition = false;
            }
            else{
                if(guess < number)
                    System.out.println("Sorry my number is higher try again or write -1 to quit ");
                else if(guess > number)
                    System.out.println("Sorry my number is lesser try again or write -1 to quit ");
            }
        }
        while(condition == true);
       
		
		
		reader.close(); //Close the resource before exiting
	}
	
	
}
