public class GCDRec{
    public static int GCD(int number1,int number2){

        int rem = 1;
        rem = number1 % number2;
        number1 = number2;
        number2 = rem;
        if(rem == 0)
            return number1;
        return GCD(number1,number2);

    }

    public static void main(String[] args){

        int number1 = Integer.parseInt(args[0]);
        int number2 = Integer.parseInt(args[1]);
        System.out.println(GCD(number1,number2));
        
    }
}           
